
## README

### device_xiaomi_daisy
Device tree based on Zidan44 his PE trees.

Fixed by Lacia-chan (aka me) with the help of Nysacape, Lunarixus, Svlog, Melvin, and Acras (and all other people that helped me)

### vendor_xiaomi_daisy

Vendor tree based on Zidan44 his PE vendor.

Fixed by Lacia-chan (aka me) with the help of Nysacape, Lunarixus, Svlog, Melvin, and Acras (and all other people that helped me)

### kernel_xiaomi_cardinal

A kernel based on ALS with the latest MSM8953 CAF tag merged into it, This kernel is meant to be clean and stable while still offering the feature's the users want (with some safety procautions)

Created by Lacia-chan (aka me) with the help of Nysacape, Lunarixus and the others that helped me!

### Bugs:
- Pictures taken in low light may show slight discoleration (com.android.camera2)



Depracetated / Dead projects 

~~### kernel_xiaomi_chunchunmaru-
My own kernel based on Android Linux Stable with the latest MSM8953 CAF tag merged in.
This kernel has a minimal feature set and is meant to be used as a clean stock kernel for custom roms.~~
